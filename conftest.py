import pytest
from conf.environment_conf import ENVIRONMENT as conf
from page_objects.PageFactory import PageFactory

@pytest.fixture(params=['attr1','attr2'])
def begin(request):
    a = request.param
    print(f'{a}')



@pytest.fixture
def adminsignup():

    tes_obj = PageFactory.get_page_object('login_page')

    tes_obj.register_driver(os_name='windows', os_version='10', browser='chrome',
                            browser_version='78.0.3904.108')
    tes_obj.setUserName(conf['uat']['AdminUserName'])
    tes_obj.setUserPwd(conf['uat']['AdminPassword'])
    tes_obj.clickLoginButton()
    return tes_obj







