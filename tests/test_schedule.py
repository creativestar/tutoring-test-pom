from page_objects.PageFactory import PageFactory
# from utils.email_util import Email_Util
from utils.database_util import Database_Util
import pytest
from datetime import datetime,timedelta,timezone

class Test_Schedule:
    def test_create_booking(self,adminsignup):
        self.test_object = PageFactory.get_page_object('calendar_page')
        if self.test_object:
            result_flag = True

        # 创建课程之前先删除数据
        self.db = Database_Util()
        result_flag &= self.db.delete_course()

        # 创建课程（创建一节今天的课程，该case不支持跨天测试）
        # 不设置FROM1 TO1,默认就是今天

        teacher = 'teacher use for auto test not use'
        student = 'student use for auto test not use'
        subject = 'College Application'
        full_from_time = (datetime.now(timezone.utc)+timedelta(minutes=10)).strftime("%Y-%m-%d %H:%M")
        from_time =  (datetime.now()+timedelta(minutes=10)).strftime('%H:%M')
        to_time = (datetime.now()+timedelta(minutes=15)).strftime('%H:%M')
        result_flag &= self.test_object.clickAddBookingButton()
        # result_flag &= self.test_object.setFromDate()
        result_flag &= self.test_object.setFromTime(from_time)
        # result_flag &= self.test_object.setToDate()
        result_flag &= self.test_object.setToTime(to_time)
        result_flag &= self.test_object.setTeacher(teacher=teacher)
        result_flag &= self.test_object.setStudent(student=student)
        result_flag &= self.test_object.setSubject(subject=subject)
        result_flag &= self.test_object.clickConfrimBookingButton()




        # 查看课程在view中的显示
        # element1 = '14:00 — 14:10: English as Second Language'这个是根据创建的课程的时间和名字来定的
        # 把element作为一个dict来传，包含了一堂课对应的几种信息
        # element2 = '14:00 English as Second Language'
        time1 = f'{from_time} - {to_time}'
        time2 = f'{from_time} — {to_time}'

        element1 = f'{time2}: {subject}'
        element2 = f'{from_time} {subject}'
        element3 = {'time':time1,'subject':subject,'teacher':teacher,'student':student}

        result_flag &= self.test_object.clickListButton()
        result_flag &= self.test_object.setDateInListView(button='Today')
        result_flag &= self.test_object.changePageInListView(page=-1)
        result_flag &= self.test_object.listDisplay(**element3)

        result_flag &= self.test_object.clickMonthButton()
        result_flag &= self.test_object.monthDisplay(element2)

        result_flag &= self.test_object.clickDayButton()
        result_flag &= self.test_object.dayDisplay(element1)


        # 上课(学生，老师) 模拟zoom调接口
        uid,uuid = self.db.get_meetingid_meetinguuid(full_from_time)
        result_flag &= self.test_object.changeClassStatus('teacher',uid,uuid,'join')
        result_flag &= self.test_object.changeClassStatus('student', uid, uuid, 'join')
#         课程结束后，判断状态是否为attended
        result_flag &= self.test_object.clickCourseInDayView(element1)
        result_flag &= self.test_object.getCourseStatus(status='Attended')
        self.db.delete_course(the_time=full_from_time)


#         删除课程

        assert result_flag, f'test fail {__file__}'
# 写断言


if __name__ == '__main__':
    pytest.main()










