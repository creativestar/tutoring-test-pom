from conf.environment_conf import ENVIRONMENT as conf
from page_objects.PageFactory import PageFactory
import time

import pytest

class Test_User_Sign_Up:

    # 这个数据是写死了的，后面要进行数据分层
    logindata = [
        (conf['uat']['BaseAdminURL'],'a4@qq.com','Star2019*'),
        (conf['uat']['BaseTeacherURL'],'t4@qq.com','Star2019*'),
        (conf['uat']['BaseStudentURL'],'s4@qq.com','Star2019*'),
        (conf['uat']['BaseStudentURL'],'p4@qq.com','Star2019*'),
    ]

    @pytest.mark.parametrize('url,username,userpwd',logindata)
    def test_admin_sign_up(self,url,username,userpwd):
        try:
            tes_obj = PageFactory.get_page_object('login_page',base_url=url)

            tes_obj.register_driver(os_name='windows', os_version='10', browser='chrome',
                                    browser_version='78.0.3904.108')
            tes_obj.setUserName(username)
            tes_obj.setUserPwd(userpwd)
            tes_obj.clickLoginButton()

            tes_obj.wait(3)
            tes_obj.logout()

            expected_pass = tes_obj.result_counter
            actual_pass = tes_obj.pass_counter

            tes_obj.teardown()


        except Exception as e:
            print(f'Exception when trying to run test :{__file__}')
            print(f'Python says: {str(e)}')

        assert expected_pass == actual_pass, f'Test failed: {__file__}'


if __name__ == '__main__':
    pytest.main()