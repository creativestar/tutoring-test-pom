import pytest
from page_objects.PageFactory import PageFactory
from utils.email_util import Email_Util
from utils.database_util import Database_Util
from utils.oktalib import oktalib

class Test_User_Management:
    email = 'autotest@qq.com'

    def test_add_a_admin_user(self,adminsignup):
        result_flag = True
        self.test_obj = PageFactory.get_page_object('user_page')
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.clickAddAUserButton()
        result_flag &= self.test_obj.setEmail(self.email)
        result_flag &= self.test_obj.setFirstName('automatic test account')
        result_flag &= self.test_obj.setLastName('automatic test account')
        result_flag &= self.test_obj.setRole('admin')

        # delete all email in inbox before register
        self.mail_obj = Email_Util()
        self.mail_obj.delete_all_email()

        # send email to inbox
        result_flag &= self.test_obj.clickAddUserButton()
        # self.test_obj.clickRensendInviteButton()
        # search related subject email
        # 创建时会发送一封，resend1次，如果都没有收到，就报错收不到邮件
        email_subject = 'Welcome to join  CreativeK12'
        uid = self.mail_obj.get_latest_email_uid(email_subject,time_delta=30)

        if uid is None:
            self.test_obj.clickRensendInviteButton()
            uid = self.mail_obj.get_latest_email_uid(email_subject)

        if uid is None:
            raise Exception ('there is no related register emai recieved')
        else:
            email_body = self.mail_obj.fetch_email_body(uid)

#         提取href
        url = self.mail_obj.get_register_href_in_body(email_body)

        # register
        if url is not None:
            self.test_obj = PageFactory.get_page_object('register_page',url)
        else:
            raise Exception ('no url found, please check the pattern')

        i = 0
        while self.test_obj.checkUrlValidation() and i<5:
            uid = self.mail_obj.get_latest_email_uid(email_subject,time_delta=30)
            email_body = self.mail_obj.fetch_email_body(uid)
            url = self.mail_obj.get_register_href_in_body(email_body)
            self.test_obj.start(url)
            i += 1


        if self.test_obj.checkUrlValidation():
            raise Exception ('there is no valid register email received')


        password = 'Star2019*'
        comfirmpassword = 'Star2019*'
        timezone = 'Beijing'
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.setUserPassword(password)
        result_flag &= self.test_obj.setUserConfirmPassword(comfirmpassword)
        result_flag &= self.test_obj.setTimeZone(timezone)
        result_flag &= self.test_obj.clickRegisterButton()

        # 删除okta数据
        self.okta = oktalib("https://dev-135159-admin.okta.com","00FO11z1RzPoLOyo0LX-k_Vip0NE42adCJvh3BCmwK")
        self.okta.connect_okta()
        result_flag &= self.okta.delete_user(self.email)

        # 删除DB数据
        self.db = Database_Util()
        result_flag &= self.db.delete_user(self.email)


        email_subject = 'Registration completed'

        uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=60)
        if uid is None:
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=60)


        assert uid is not None,f'Test fail {__file__}'

        # 最后把邮件全部删除
        self.mail_obj.delete_all_email()
        self.test_obj.teardown()

    def test_add_a_student_user(self,adminsignup):
        result_flag = True
        self.test_obj = PageFactory.get_page_object('user_page')
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.clickAddAUserButton()
        result_flag &= self.test_obj.setEmail(self.email)
        result_flag &= self.test_obj.setFirstName('automatic test account')
        result_flag &= self.test_obj.setLastName('automatic test account')
        result_flag &= self.test_obj.setRole('student')
        result_flag &= self.test_obj.setStudentPurchasedHours(100.5)
        result_flag &= self.test_obj.setStudentSubject('subject')
        result_flag &= self.test_obj.setStudentTeacher('teacher')

        # delete all email in inbox before register
        self.mail_obj = Email_Util()
        self.mail_obj.delete_all_email()

        # send email to inbox
        result_flag &= self.test_obj.clickAddUserButton()
        # self.test_obj.clickRensendInviteButton()
        # search related subject email
        # 创建时会发送一封，resend1次，如果都没有收到，就报错收不到邮件
        email_subject = 'Welcome to join  CreativeK12'
        uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)

        if uid is None:
            self.test_obj.clickRensendInviteButton()
            uid = self.mail_obj.get_latest_email_uid(email_subject)

        if uid is None:
            raise Exception('there is no related register emai recieved')
        else:
            email_body = self.mail_obj.fetch_email_body(uid)

        #         提取href
        url = self.mail_obj.get_register_href_in_body(email_body,'student')

        # register
        self.test_obj = PageFactory.get_page_object('register_page', url)

        i = 0
        while self.test_obj.checkUrlValidation() and i < 5:
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            email_body = self.mail_obj.fetch_email_body(uid)
            url = self.mail_obj.get_register_href_in_body(email_body)
            self.test_obj.start(url)
            i += 1

        if self.test_obj.checkUrlValidation():
            raise Exception('there is no valid register email received')

        password = 'Star2019*'
        comfirmpassword = 'Star2019*'
        timezone = 'Beijing'
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.setUserPassword(password)
        result_flag &= self.test_obj.setUserConfirmPassword(comfirmpassword)
        result_flag &= self.test_obj.setTimeZone(timezone)
        result_flag &= self.test_obj.clickRegisterButton()

        # 删除okta数据
        self.okta = oktalib("https://dev-135159-admin.okta.com", "00FO11z1RzPoLOyo0LX-k_Vip0NE42adCJvh3BCmwK")
        self.okta.connect_okta()
        result_flag &= self.okta.delete_user(self.email)

        # 删除DB数据
        self.db = Database_Util()
        result_flag &= self.db.delete_user(self.email)

        email_subject = 'Registration completed'
        email_subject1 = 'New Student Joined'

        receive_email_flag = 0
        receive_email_flag1 = 0

        for i in range(2):
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            if uid is not None:
                receive_email_flag = 1
                break


        for i in range(2):
            uid = self.mail_obj.get_latest_email_uid(email_subject1,time_delta=30)
            if uid is not None:
                receive_email_flag1 = 1
                break

        result_flag &= receive_email_flag
        result_flag &= receive_email_flag1


        assert result_flag, f'Test fail {__file__}'

        # 最后把邮件全部删除
        self.mail_obj.delete_all_email()
        self.test_obj.teardown()

    def test_add_a_teacher_user(self, adminsignup):
        result_flag = True
        self.test_obj = PageFactory.get_page_object('user_page')
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.clickAddAUserButton()
        result_flag &= self.test_obj.setEmail(self.email)
        result_flag &= self.test_obj.setFirstName('automatic test account')
        result_flag &= self.test_obj.setLastName('automatic test account')
        result_flag &= self.test_obj.setRole('teacher')
        result_flag &= self.test_obj.setTeacherSalutation('salutation')
        result_flag &= self.test_obj.setTeacherSuject('subject')

        # delete all email in inbox before register
        self.mail_obj = Email_Util()
        self.mail_obj.delete_all_email()

        # send email to inbox
        result_flag &= self.test_obj.clickAddUserButton()
        # self.test_obj.clickRensendInviteButton()
        # search related subject email
        # 创建时会发送一封，resend1次，如果都没有收到，就报错收不到邮件
        email_subject = 'Welcome to join  CreativeK12'
        uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)

        if uid is None:
            self.test_obj.clickRensendInviteButton()
            uid = self.mail_obj.get_latest_email_uid(email_subject)

        if uid is None:
            raise Exception('there is no related register emai recieved')
        else:
            email_body = self.mail_obj.fetch_email_body(uid)

        #         提取href
        url = self.mail_obj.get_register_href_in_body(email_body, 'teacher')

        # register
        self.test_obj = PageFactory.get_page_object('register_page', url)

        i = 0
        while self.test_obj.checkUrlValidation() and i < 5:
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            email_body = self.mail_obj.fetch_email_body(uid)
            url = self.mail_obj.get_register_href_in_body(email_body)
            self.test_obj.start(url)
            i += 1

        if self.test_obj.checkUrlValidation():
            raise Exception('there is no valid register email received')

        password = 'Star2019*'
        comfirmpassword = 'Star2019*'
        timezone = 'Beijing'
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.setUserPassword(password)
        result_flag &= self.test_obj.setUserConfirmPassword(comfirmpassword)
        result_flag &= self.test_obj.setTimeZone(timezone)
        result_flag &= self.test_obj.clickRegisterButton()

        # 删除okta数据
        self.okta = oktalib("https://dev-135159-admin.okta.com", "00FO11z1RzPoLOyo0LX-k_Vip0NE42adCJvh3BCmwK")
        self.okta.connect_okta()
        result_flag &= self.okta.delete_user(self.email)

        # 删除DB数据
        self.db = Database_Util()
        result_flag &= self.db.delete_user(self.email)

        email_subject = 'Registration completed'
        email_subject1 = 'New Teacher Joined'

        receive_email_flag = 0
        receive_email_flag1 = 0

        for i in range(2):
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            if uid is not None:
                receive_email_flag = 1
                break

        for i in range(2):
            uid = self.mail_obj.get_latest_email_uid(email_subject1, time_delta=30)
            if uid is not None:
                receive_email_flag1 = 1
                break

        result_flag &= receive_email_flag
        result_flag &= receive_email_flag1

        assert result_flag, f'Test fail {__file__}'

        # 最后把邮件全部删除
        self.mail_obj.delete_all_email()
        self.test_obj.teardown()

    def test_add_a_teacher_user(self, adminsignup):
        result_flag = True
        self.test_obj = PageFactory.get_page_object('user_page')
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.clickAddAUserButton()
        result_flag &= self.test_obj.setEmail(self.email)
        result_flag &= self.test_obj.setFirstName('automatic test account')
        result_flag &= self.test_obj.setLastName('automatic test account')
        result_flag &= self.test_obj.setRole('parent')
        result_flag &= self.test_obj.setParentConnectToExsistingStudent('student')


        # delete all email in inbox before register
        self.mail_obj = Email_Util()
        self.mail_obj.delete_all_email()

        # send email to inbox
        result_flag &= self.test_obj.clickAddUserButton()
        # self.test_obj.clickRensendInviteButton()
        # search related subject email
        # 创建时会发送一封，resend1次，如果都没有收到，就报错收不到邮件
        email_subject = 'Welcome to join  CreativeK12'
        uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)

        if uid is None:
            self.test_obj.clickRensendInviteButton()
            uid = self.mail_obj.get_latest_email_uid(email_subject)

        if uid is None:
            raise Exception('there is no related register emai recieved')
        else:
            email_body = self.mail_obj.fetch_email_body(uid)

        #         提取href
        url = self.mail_obj.get_register_href_in_body(email_body, 'parent')

        # register
        self.test_obj = PageFactory.get_page_object('register_page', url)

        i = 0
        while self.test_obj.checkUrlValidation() and i < 5:
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            email_body = self.mail_obj.fetch_email_body(uid)
            url = self.mail_obj.get_register_href_in_body(email_body)
            self.test_obj.start(url)
            i += 1

        if self.test_obj.checkUrlValidation():
            raise Exception('there is no valid register email received')

        password = 'Star2019*'
        comfirmpassword = 'Star2019*'
        timezone = 'Beijing'
        if self.test_obj is not None:
            result_flag = True
        result_flag &= self.test_obj.setUserPassword(password)
        result_flag &= self.test_obj.setUserConfirmPassword(comfirmpassword)
        result_flag &= self.test_obj.setTimeZone(timezone)
        result_flag &= self.test_obj.clickRegisterButton()

        # 删除okta数据
        self.okta = oktalib("https://dev-135159-admin.okta.com", "00FO11z1RzPoLOyo0LX-k_Vip0NE42adCJvh3BCmwK")
        self.okta.connect_okta()
        result_flag &= self.okta.delete_user(self.email)

        # 删除DB数据
        self.db = Database_Util()
        result_flag &= self.db.delete_user(self.email)

        email_subject = 'Registration completed'


        receive_email_flag = 0

        for i in range(2):
            uid = self.mail_obj.get_latest_email_uid(email_subject, time_delta=30)
            if uid is not None:
                receive_email_flag = 1
                break

        result_flag &= receive_email_flag


        assert result_flag, f'Test fail {__file__}'

        # 最后把邮件全部删除
        self.mail_obj.delete_all_email()
        self.test_obj.teardown()


if __name__ == '__main__':
    pytest.main()




