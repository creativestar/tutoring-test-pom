import psycopg2
import conf.database_conf as conf_file


class Database_Util:

    def connect_db(self):
        dbHost = conf_file.host
        dbPort = conf_file.port
        dbUser = conf_file.username
        dbPass = conf_file.password
        Dbdatabase = conf_file.database
        db = psycopg2.connect(database=Dbdatabase, user=dbUser, password=dbPass, host=dbHost,
                              port=dbPort)
        return db



    def find_one(self, sql):
        db = self.connect_db()
        cur = db.cursor()
        cur.execute(sql)
        # res = cur.fetchone()
        res = cur.fetchall()
        db.close()
        return res

    def delete_user(self,email):
        # 拿到user_id
        select_user_id_sql = conf_file.select_user_id_sql%email
        user_id = self.find_one(select_user_id_sql)
        user_id = user_id[0][0]

        delete_user_in_chatlist_sql = conf_file.delete_user_in_chatlist_sql%(user_id,user_id)
        delete_user_in_user_role_sql = conf_file.delete_user_in_user_role_sql%user_id
        delete_user_in_user_sql = conf_file.delete_user_in_user_sql%user_id

        db = self.connect_db()
        cur = db.cursor()
        cur.execute(delete_user_in_chatlist_sql)
        cur.execute(delete_user_in_user_role_sql)
        db.commit()

        cur.execute(delete_user_in_user_sql)
        db.commit()

        res = self.find_one(select_user_id_sql)
        if res == '':
            result = True
        else:
            result = False
        db.close()
        return result
