# -*- coding: utf-8 -*-
from okta import UsersClient
# import approot


class oktalib(object):

    def __init__(self, url, token):
        self.url = url
        self.token = token
        self.server = None

    #连接okta
    def connect_okta(self):
        try:
            self.server = UsersClient(self.url, self.token)
            return self.server
        except BaseException as e:
            print(e)

    #check if exist user and return status
    def get_user_status(self,user):
        #user is email format
        try:
            user = self.server.get_user(user)
            return user.status
        except BaseException as e:
            print(e)

    def delete_user(self, user):
        status = self.get_user_status(user)
        i = 0
        while status and i<10:
            self.server.delete_user(user)
            status = self.get_user_status(user)
            i += 1

        if status:
            return False
        else:
            return True

if __name__ == '__main__':
    oktaClient = oktalib("https://dev-135159-admin.okta.com","00FO11z1RzPoLOyo0LX-k_Vip0NE42adCJvh3BCmwK")
    oktaClient.connect_okta()
    a = oktaClient.delete_user("admintest@qq.com")
    print(a)
