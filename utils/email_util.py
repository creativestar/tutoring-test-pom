"""
A simple IMAP util that will help us with account activation
* Connect to your imap host
* Login with username/password
* Fetch latest messages in inbox
* Get a recent registration message
* Filter based on sender and subject
* Return text of recent messages

[TO DO](not in any particular order) 
1. Extend to POP3 servers
2. Add a try catch decorator
3. Enhance get_latest_email_uid to make all parameters optional
"""
#The import statements import: standard Python modules,conf
import os,sys,time,imaplib,email,datetime
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import conf.email_conf as conf_file
import re,bs4


class Email_Util:
    "Class to interact with IMAP servers"
    destmailbox = 'BAK'

    def connect(self,imap_host = conf_file.imaphost):
        "Connect with the host"
        self.mail = imaplib.IMAP4_SSL(imap_host)
        
        return self.mail


    def login(self,username = conf_file.username,password = conf_file.app_password):
        "Login to the email"
        result_flag = False
        try:
            self.mail.login(username,password)
        except Exception as e:
            print('\nException in Email_Util.login')
            print('PYTHON SAYS:')
            print(e)
            print('\n')
        else:
            result_flag = True

        return result_flag


    def get_folders(self):
        "Return a list of folders"
        return self.mail.list()

    def set_flag_SEEN(self):
        result, data = self.mail.search(None, '(UNSEEN)')
        for i in data[0].split():
            self.mail.store(i,'+FLAGS','(SEEN)')

    def create_folder(self,folder):
        self.connect()
        self.login()
        self.get_folders()
        # if self.select_folder("&UXZO1mWHTvZZOQ-/BAK"):
        #     print('found the folder')
        is_floder_exist = self.mail.select("&UXZO1mWHTvZZOQ-/BAK")
        if is_floder_exist[0] != 'OK':
            return self.mail.create(folder)
        else:
            return True

    def select_folder(self,folder='INBOX'):
        "Select the given folder if it exists. E.g.: [Gmail]/Trash"
        # self.connect()
        # self.login()
        # self.get_folders()
        result_flag = False
        response = self.mail.select(folder)
        if response[0] == 'OK':
            result_flag = True

        return result_flag

    def delete_all_email(self): #删除当前登录账号所有邮件
        #判断是否有BAK文件夹,没有则创建
        self.create_folder(self.destmailbox)
        self.select_folder('INBOX')
        i = 0
        all_uid = []
        while i < 3 and len(all_uid) == 0:
            i += 1
            result,data = self.mail.search(None,'(all)')
            all_uid = data[0].split()

        if len(all_uid) == 0:
            print('there is no message in INBOX')

        p = 0
        while len(all_uid) and p<5:
            for x in all_uid:
                result = self.mail.copy(x , "&UXZO1mWHTvZZOQ-/BAK")

                if result[0] == 'OK':
                    self.mail.store(x,'+FLAGS','(\\Deleted)')
                else:
                    print(f'email {x} cannot be copied')

            result, data = self.mail.search(None, '(all)')
            all_uid = data[0].split()
            p += 1

        return  self.mail.expunge()






    def get_latest_email_uid(self,search_string,time_delta=10,wait_time=300):
        "Search for a subject and return the latest unique ids of the emails"
        # 先查找UNSEED邮件，拿取每一封的内容，把当中subject符合条件的邮件uid抓出来
        # 再把最后一封邮件uid返回
        uid = []
        time_elapsed = 0
        while (time_elapsed < wait_time and uid == []):
            time.sleep(time_delta)
            result, data = self.mail.search(None, "(UNSEEN)")
            all_uid = data[0].split()
            for i in all_uid:
                result, data = self.mail.fetch(i, '(RFC822)')
                raw_email = data[0][1]
                email_msg = email.message_from_bytes(raw_email)
                subject = email.header.decode_header(email_msg["Subject"])

                if subject[0][0] == search_string:
                    uid.append(i)

            time_elapsed += 1

        if len(uid):
            return uid[-1]
        else:
            return None


    def fetch_email_body(self,uid):
        "Fetch the email body for a given uid"
        # 返回一个message对象，这个对象就是要获取的email的body
        if uid is not None:
            result,data = self.mail.fetch(uid,'(RFC822)')
            raw_email = data[0][1]
            email_msg = email.message_from_bytes(raw_email)

        return email_msg
        
    #占时没有用这个，这个函数解析出来在href中有换行符
    # def get_email_body(self,email_msg):
    #     "Parse out the text of the email message. Handle multipart messages"
    #     email_body = []
    #     maintype = email_msg.get_content_maintype()
    #     if maintype == 'multipart':
    #         for part in email_msg.get_payload():
    #             if part.get_content_maintype() == 'text':
    #                 email_body.append(part.get_payload())
    #     elif maintype == 'text':
    #         email_body.append(email_msg.get_payload())
    #
    #     return email_body

    def get_charset(self,message):
        #Get the message charset
        return message.get_charset()

    def get_register_href_in_body(self,email_body):
        res = []

        for part in email_body.walk():
            if  len(res) == 0 and part.is_multipart() is False:
                # contenttype = part.get_content_type()
                charset = self.get_charset(part)

                if charset == None:
                    mailContent = part.get_payload(decode=True)
                else:
                    mailContent = part.get_payload(decode=True).decode(charset)

                # pat = re.compile(r"(http\:\/\/uattutoring\.creativek12\.com\/user\/register\/(admin|student|teacher|parent)\/\w+\/\d+)")
                pat = re.compile(r"^http://\w+.creativek12.com/user/register/admin/\w+\=\=/\d+$")
                # 因为mailcontent中不是单纯的byte或者str，而是html，所以要先把对应的str解析出来

                htmlcontent = bs4.BeautifulSoup(mailContent,'html5lib')
                if type(htmlcontent) is not None:
                    tag_a = htmlcontent.select('a')
                    for i in tag_a:
                        res = re.findall(pat,i['href'])
                        if len(res):
                            break

            elif len(res)>0 :
                break

        return res[0]

        #
        # href = {}
        # email_body = BeautifulSoup(email_body,'html5lib')
        # for k in email_body.find_all('a'):
        #     href.k = k['href']
        # return href


    def logout(self):
        "Logout"
        result_flag = False
        response, data = self.mail.logout()
        if response == 'BYE':
            result_flag = True

        return result_flag


#---EXAMPLE USAGE---
if __name__=='__main__':
    #Fetching conf details from the conf file
    imap_host = conf_file.imaphost
    username = conf_file.username
    password = conf_file.app_password

    #Initialize the email object
    email_obj = Email_Util()

    #Connect to the IMAP host
    email_obj.connect(imap_host)
    
    #Login
    if email_obj.login(username,password):
        print("PASS: Successfully logged in.")
    else:
        print("FAIL: Failed to login")

    #Get a list of folder
    folders = email_obj.get_folders() 
    if folders != None or []:
        print("PASS: Email folders:", email_obj.get_folders()) 

    else:
        print("FAIL: Didn't get folder details")

    #Select a folder
    if email_obj.select_folder('Inbox'):
        print("PASS: Successfully selected the folder: Inbox")
    else:
        print("FAIL: Failed to select the folder: Inbox")
    
    #Get the latest email's unique id
    uid = email_obj.get_latest_email_uid(wait_time=300)
    if uid != None:
        print("PASS: Unique id of the latest email is: ",uid)
    else:
        print("FAIL: Didn't get unique id of latest email")
    
    #A. Look for an Email from provided sender, print uid and check it's contents
    # uid = email_obj.get_latest_email_uid(wait_time=300)
    # if uid != None:
    #     print("PASS: Unique id of the latest email with given sender is: ",uid)
    #
    #     #Check the text of the latest email id
    #     email_body = email_obj.fetch_email_body(uid)
    #     data_flag = False
    #     print("  - Automation checking mail contents")
    #     for line in email_body:
    #         line = line.replace('=','')
    #         line = line.replace('<','')
    #         line = line.replace('>','')
    #
    #         if "Hi Email_Util" and "This email was sent to you" in line:
    #             data_flag = True
    #             break
    #     if data_flag == True:
    #         print("PASS: Automation provided correct Email details. Email contents matched with provided data.")
    #     else:
    #         print("FAIL: Provided data not matched with Email contents. Looks like automation provided incorrect Email details")
    #
    # else:
    #     print("FAIL: After wait of 5 mins, looks like there is no email present with given sender")
    #
    # #B. Look for an Email with provided subject, print uid, find Qxf2 POM address and compare with expected address
    # uid = email_obj.get_latest_email_uid(subject="Qxf2 Services: Public POM Link",wait_time=300)
    # if uid != None:
    #     print("PASS: Unique id of the latest email with given subject is: ",uid)
    #     #Get pom url from email body
    #     email_body = email_obj.fetch_email_body(uid)
    #     expected_pom_url = "https://github.com/qxf2/qxf2-page-object-model"
    #     pom_url = None
    #     data_flag = False
    #     print("  - Automation checking mail contents")
    #     for body in email_body:
    #         search_str = "/qxf2/"
    #         body = body.split()
    #         for element in body:
    #             if search_str in element:
    #                 pom_url = element
    #                 data_flag = True
    #                 break
    #
    #         if data_flag == True:
    #             break
    #
    #     if data_flag == True and expected_pom_url == pom_url:
    #         print("PASS: Automation provided correct mail details. Got correct Qxf2 POM url from mail body. URL: %s"%pom_url)
    #     else:
    #         print("FAIL: Actual POM url not matched with expected pom url. Actual URL got from email: %s"%pom_url)
    #
    # else:
    #     print("FAIL: After wait of 5 mins, looks like there is no email present with given subject")
    #
    # #C. Look for an Email with provided sender and subject and print uid
    # uid = email_obj.get_latest_email_uid(subject="get more out of your new Google Account",sender="andy-noreply@google.com",wait_time=300)
    # if uid != None:
    #     print("PASS: Unique id of the latest email with given subject and sender is: ",uid)
    # else:
    #     print("FAIL: After wait of 5 mins, looks like there is no email present with given subject and sender")
    #
    # #D. Look for an Email with non-existant sender and non-existant subject details
    # uid = email_obj.get_latest_email_uid(subject="Activate your account",sender="support@qxf2.com",wait_time=120) #you can change wait time by setting wait_time variable
    # if uid != None:
    #     print("FAIL: Unique id of the latest email with non-existant subject and non-existant sender is: ",uid)
    # else:
    #     print("PASS: After wait of 2 mins, looks like there is no email present with given non-existant subject and non-existant sender")
    #
