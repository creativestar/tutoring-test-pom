from .Base_Page import Base_Page
from .header_object import Header_Object
import conf.locators_conf as locators

class Register_Page(Base_Page,Header_Object):
    def start(self,url = ''):
        if url == '':
            result_flag = self.driver.get(self.base_url)
        else:
            result_flag = self.driver.get(url)
        return result_flag

    def setUserPassword(self,password):
        result_flag = self.set_text(locators.register_password_field,password)
        return result_flag

    def setUserConfirmPassword(self,confirmpassword):
        result_flag = self.set_text(locators.register_confirm_password_field,confirmpassword)
        return result_flag

    def setTimeZone(self,timezone='Beijing'):
        result_flag = self.select_dropdown_option_js(locators.register_time_zone_field,locators.register_time_zone_option%timezone)
        return result_flag
    
    def clickRegisterButton(self):
        result_flag = self.click_element(locators.register_button)
        return result_flag

    def checkUrlValidation(self):
        result_flag = self.get_element(locators.register_invalid_page)
        return result_flag
