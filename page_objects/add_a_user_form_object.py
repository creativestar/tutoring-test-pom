import conf.locators_conf as locators

class Add_A_User_Form_Object():
    def setEmail(self,email):
        return self.set_text(locators.email_field,email)

    def setFirstName(self,firstname):
        return self.set_text(locators.first_name_field,firstname)

    def setLastName(self,lastname):
        return self.set_text(locators.last_name_field, lastname)

    def setPhoneFlag(self,flag):
        pass

    def setPhoneNumber(self,phone):
        return self.set_text(locators.phone_number,phone)

    def setWeChat(self,wechart):
        return self.set_text(locators.wechat_field, wechart)

    def setOrganization(self,organ):
        return self.set_text(locators.organzation_field, organ)

    def setRole(self,role):
        if role == 'student':
            result_flag = self.click_element(locators.student_role)
        elif role == 'parent':
            result_flag = self.click_element(locators.parent_role)
        elif role == 'teacher':
            result_flag = self.click_element(locators.teacher_role)
        elif role == 'admin':
            result_flag = self.click_element(locators.admin_role)

        return result_flag

    # student
    def setPurchasedHours(self,hour):
        pass

    def setSubject(self,subject):
        pass

    def setTeacher(self,teacher):
        pass

    def setConnectToExsistingParent(self,parent):
        pass

    # parent
    def setConnectToExsistingStudent(self,student):
        pass

    # teacher
    def setSalutation(self,salutation):
        pass

    def setRanking(self,ranking):
        pass

    def setTeacherSuject(self,subject):
        pass

    # admin

    def clickAddUserButton(self):
        result_flag =  self.click_element(locators.form_object_add_a_user_button)
        return result_flag

    def clickCancelButton(self):
        pass

    def clickXButton(self):
        pass

