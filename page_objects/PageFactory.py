"""
PageFactory uses the factory design pattern. 
get_page_object() returns the appropriate page object.
Add elif clauses as and when you implement new pages.
Pages implemented so far:
1. Login Page
"""
from page_objects.login_page import Login_Page
from page_objects.user_page import User_Page
from page_objects.register_page import Register_Page
from conf.environment_conf import ENVIRONMENT as conf



class PageFactory():
    "PageFactory uses the factory design pattern."
    def get_page_object(page_name,base_url = conf['uat']['BaseAdminURL']):
        "Return the appropriate page object based on page_name"
        test_obj = None
        page_name = page_name.lower()

        if page_name == "login_page":
            test_obj = Login_Page(base_url=base_url)

        elif page_name == "user_page":
            test_obj = User_Page(base_url = base_url)

        elif page_name == "register_page":
            test_obj = Register_Page(base_url = base_url)

        return test_obj

    get_page_object = staticmethod(get_page_object)