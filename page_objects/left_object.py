import conf.locators_conf as locators

class Left_Object():
    def switchPage(self,pagename):
        if pagename.lower() == 'calendar':
            self.select_dropdown_option(locators.left_list_option_schedule,locators.left_list_option_calendar)

        elif pagename.lower() == 'users':
            self.click_element(locators.left_list_option_users)

        elif pagename.lower() == 'reports':
            self.click_element(locators.left_list_option_reports)

        elif pagename.lower() == 'logs':
            self.click_element(locators.left_list_option_logs)






