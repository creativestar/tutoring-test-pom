from .Base_Page import Base_Page
from .header_object import Header_Object
from .left_object import Left_Object
from .add_a_user_form_object import Add_A_User_Form_Object
import conf.locators_conf as locators


class User_Page(Base_Page,Header_Object,Left_Object,Add_A_User_Form_Object):
    def start(self):
        return self.click_element(locators.left_list_option_users)

    def clickAddAUserButton(self):
        result_flag = self.click_element(locators.user_page_add_a_user_button)
        return result_flag

    def clickActionOption(self,option):
        """
            option = 1 --edit user,
                    2 --show user booking,
                    3 --edit availability,
                    4 --reset password,
                    5 --deactivate user,
                    6 --reactivate user,
                    7 --remove user

        """
        if option == 1:
            pass
        elif option  == 2:
            pass
        elif option == 3:
            pass
        elif option == 4:
            pass
        elif option == 5:
            pass
        elif option == 6:
            pass
        elif option == 7:
            pass


    def clickRensendInviteButton(self):
        result_flag = self.click_element(locators.resend_invite_button)
        return result_flag








