from .Base_Page import Base_Page
from .header_object import Header_Object
import conf.locators_conf as locators


class Login_Page(Base_Page,Header_Object):
    def start(self):
        result_flag = self.driver.get(self.base_url)
        return result_flag


    def setUserName(self,userName):
        # this structure is a little complex
        try:
            result_flag = None
            if self.set_text(locators.username_field,userName):
                result_flag = True
            else:
                result_flag = False

            self.conditional_write(result_flag,
                                   positive='Pass to set text of UserName',
                                   negative='Failed to set text of UserName',
                                   level='debug')
        except Exception as e:
            self.write("Exception while set text of UserName")
            self.write(str(e))
        return result_flag


    def setUserPwd(self,userPwd):
        result_flag = self.set_text(locators.password_field, userPwd)
        return result_flag


    def checkRememberMe(self, status):
        if status:
            result_flag = self.select_checkbox(locators.remenber_me_checkbox)
        else:
            result_flag = self.deselect_checkbox(locators.remenber_me_checkbox)
        return result_flag


    def clickLoginButton(self):
        result_flag = self.click_element(locators.login_button)
        return result_flag


    def clickResetPassword(self):
        result_flag = self.click_element(locators.reset_password_link)
        return result_flag


    def clickHowDoIRegister(self):
        result_flag = self.click_element(locators.how_do_i_register_link)
        return result_flag


    def clickContactUs(self):
        if self.check_element_present(locators.contact_us_link) is False:
            result_flag = self.clickHowDoIRegister()


        result_flag &= self.click_element(locators.contact_us_link)
        return result_flag









