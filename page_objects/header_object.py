import conf.locators_conf as locators

class Header_Object():
    def changePageLangeuageBeforeLogin(self,langeuage):
        if langeuage.lower() == 'english':
            if self.get_text(locators.login_page_language_field) == langeuage.lower():
                return
            else:
                result_flag = self.select_dropdown_option_js(locators.login_page_language_field,locators.english_option)

        elif langeuage.lower() == 'chinese':
            if self.get_text(locators.login_page_language_field) == langeuage.lower():
                return
            else:
                result_flag = self.select_dropdown_option_js(locators.login_page_language_field,locators.chinese_option)
        else:
            raise Exception ('only support language english and chinese, please check')

        return result_flag


    def changePageLangeuageAfterLogin(self,langeuage):
        # change the language of page after user has logined
        if langeuage.lower() == 'english':
            if self.get_text(locators.login_page_language_field) == langeuage.lower():
                return
            else:
                result_flag = self.select_dropdown_option_js(locators.login_in_langeuage_field,locators.english_option)

        elif langeuage.lower() == 'chinese':
            if self.get_text(locators.login_page_language_field) == langeuage.lower():
                return
            else:
                result_flag = self.select_dropdown_option_js(locators.login_in_langeuage_field,locators.chinese_option)
        else:
            raise Exception ('only support language english and chinese, please check')

        return result_flag


    def logout(self):
        result_flag =  self.select_dropdown_option_js(locators.profile_field,locators.logout_option)
        return result_flag

    def exportFileInUserPage(self,exportformat):
        if exportformat.lower() == 'csv':
            result_flag = self.select_dropdown_option_js(locators.export_button,locators.export_option_csv)
        elif exportformat.lower() == 'xls':
            result_flag = self.select_dropdown_option_js(locators.export_button,locators.export_option_xls)

        return result_flag

    def setSearchFieldValue(self,content):
        result_flag = self.set_text(locators.search_field,content)
        return result_flag


