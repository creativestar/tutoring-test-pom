#Common locator file for all locators
#Locators are ordered alphabetically

############################################
#Selectors we can use
#ID
#NAME
#css selector
#CLASS_NAME
#LINK_TEXT
#PARTIAL_LINK_TEXT
#XPATH
###########################################

#Locators for login page(login_page.py)
username_field = "id,userName"
password_field = "id,password"
remenber_me_checkbox = "xpath,//input[@type='checkbox']"
login_button = "xpath,//button[@type='submit']"
reset_password_link = "xpath,//span[@class = 'antd-pro-pages-user-login-resetPassword']"
how_do_i_register_link = "class,antd-pro-pages-user-login-register"
contact_us_link = "text,contact us"

#----

#Locators for calendar page(calendar_page.py)

#----

#Locators for header object, language(header_object.py)
login_page_language_field = "xpath,//span['antd-pro-components-select-lang-index-dropDown ant-dropdown-trigger']"
login_in_langeuage_field = "xpath,//span['antd-pro-components-select-lang-index-dropDown antd-pro-components-global-header-index-action ant-dropdown-trigger']"
english_option = "xpath,//ul[@role = 'menu']/li/span[contains(text(),'🇬🇧')]"
chinese_option = "xpath,//ul[@role = 'menu']/li/span[contains(text(),'🇨🇳')]"

#Locators for header object, profile(header_object.py)
profile_field = "xpath, //span[@class = 'antd-pro-components-global-header-index-action antd-pro-components-global-header-index-account ant-dropdown-trigger']"
profile_option = "xpath, //ul[@role = 'menu']/li[1]/span[contains(text(),'My Profile')]"
logout_option = "xpath,//ul[@role = 'menu']/li[2]/span[contains(text(),'Logout')]"
# user page header
export_button = ""
export_option_csv = ""
export_option_xls = ""
search_field = "xpath,//span[@class = 'ant-input-search antd-pro-pages-user-manage-user-manage-search ant-input-affix-wrapper']"
filter_button = ""
filter_option = ""



#Locators for user page(user_page.py)
user_page_add_a_user_button = "xpath,// *[ @ id = 'root'] / div / section / section / main / div[1] / div[1] / div[2] / button[1]"
resend_invite_button = 'xpath, //p[@class = ""]'


#----
#Locators for left object(left_object.py)
left_list_option_schedule = "CLASS_NAME,anticon anticon-schedule"
left_list_option_calendar = "partial_link_text, calendar"
left_list_option_users = "xpath,// *[ @ id = 'root'] / div / section / aside / div / ul / li[2] / a"
left_list_option_reports = "xpath,// *[ @ id = 'root'] / div / section / aside / div / ul / li[3] / a"
left_list_option_logs = "xpath,// *[ @ id = 'root'] / div / section / aside / div / ul / li[4] / a"


#Locators for add a user form object(add a user form object.py)
email_field = "xpath, //* [@id = 'email']"
first_name_field = "xpath, //* [@id = 'firstName']"
last_name_field = "xpath, //* [@id = 'lastName']"
phone_flag = ""
phone_flag_option = ""
phone_number = "xpath, //* [@id = 'mobilePhone']"
wechat_field = "xpath, //* [@id = 'weChat']"
organzation_field = "xpath, //* [@id = 'organization']"
admin_role = "xpath,//div [@id = 'role']/label[4]"
student_role = "xpath,//div [@id = 'role']/label[1]"
teacher_role = "xpath,//div [@id = 'role']/label[3]"
parent_role = "xpath,//div [@id = 'role']/label[2]"
form_object_add_a_user_button = "xpath,//form/button[@class = 'ant-btn confirmBtn ant-btn-primary']"



#Locators for register page(register_page.py)
register_password_field="id,password"
register_confirm_password_field="id,confirmPassword"
register_time_zone_field="xpath,//div[@class = 'ant-select-selection__placeholder']"
register_time_zone_option= "xpath,//ul[@role='listbox']/li[contains(text(),'%s')]"
register_button = "xpath,//button[@type = 'submit']"
register_invalid_page = "xpath, //p[@class = 'antd-pro-pages-user-login-linkInvalidContent']"







"""
获取第三个a标签的下一个a标签："//a[@id='3']/following-sibling::a[1]"

获取第三个a标签后面的第N个标签："//a[@id='3']/following-sibling::*[N]"

获取第三个a标签的上一个a标签："//a[@id='3']/preceding-sibling::a[1]"

获取第三个a标签的前面的第N个标签："//a[@id='3']/preceding-sibling::*[N]"

获取第三个a标签的父标签："//a[@id=='3']/.."
"""